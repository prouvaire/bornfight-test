<?php
class weather {
   private $weatherTypes = array("sunny", "snowy", "foggy");

   /* randomly chooses the type of weather 
   from the $weatherTypes array for the day of attack */
   public function getWeatherReport() {
             return $this->weatherTypes[array_rand($this->weatherTypes, 1)];
    }
}

?>
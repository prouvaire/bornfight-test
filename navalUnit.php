<?php
require_once("unit.php");

class navalUnit extends unit {
   
    public function __construct() {
        $this->setName("Naval unit");
        $this->setWeatherCoefficients ( array(
        "snowy" => 1.0,
        "sunny" => 0.8,
        "foggy" => 0.5,
            ));
       
    }

   protected function printStatement($weather) {
     switch ($weather) {
            case "snowy":
                echo("The submarines aren't bothered by the snow!");
                break;
            case "sunny":
                echo("The naval unit isn't too bothered by the sun, but there's still risk of being seen!");
                break;
            case "foggy":
                echo("The fog decreases chances of success!"); 
                break;
            default:
                echo("This type of weather isn't implemented!");
     }   
        
    }
}

?>

<?php

abstract class unit { 
    private $name;
    private $weatherCoefficients;

    abstract protected function printStatement($weather); 

    public function setWeatherCoefficients($weatherCoefficients) {
        $this->weatherCoefficients = $weatherCoefficients;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
       return $this->name;
    }

    /* chances of an attack being successful
     depend on the current weather and destructive power */
    public function calculateSuccess($weather, $destructivePower) {
        $this->printStatement($weather);

        try {
            return $this->weatherCoefficients[$weather] * $destructivePower;
         } catch (Exception $e) {
            echo ("ERROR: There's no coefficient for this type of weather! ");
        }
             
    }   
} 


?>
<?php
require_once("weather.php");
require_once("airUnit.php");
require_once("landUnit.php");
require_once("navalUnit.php");

class army {

    private $name; 
    private $size; 
    private $foodAmount; 
     

    public function __construct($name, $size) {
        $this->name = $name; 
        $this->size = $size;

        // at the beginning both armies have enough food for 10 days
        $this->foodAmount = 10 * $size;  
        $this->setUnits();
        
    }       

    public function getName() {
        return $this->name;
    }


    private function setUnits() {
        $this->units = array();
      
        $this->units[] = (new landUnit());
        $this->units[] = (new navalUnit());
        $this->units[] = (new airUnit());
    }

    public function pickUnit() {
        return $this->units[array_rand($this->units, 1)];
    }
    
    public function getSize() {
        return $this->size;
    }

    /*  if an attack is succesful, a random number 
    of soldiers from the defending army is deleted */
    public function deleteSoldiers() {
       $howMany = rand(1, $this->size);
       $this->size -= $howMany; 
   }


    /* the soldiers eat lunch at the beginning of their turn
    if there's no meals left, they will starve to death and
    the defending army automatically wins  */  
    public function eatLunch() {
        $this->foodAmount -=  $this->size;
        if ($this->foodAmount <=0) {
            $this->size =0;
            echo ("All the soldiers have lunch. The army doesn't have any meals left! The soldiers slowly starve to death...<br>");
        } else {
            echo ("All the soldiers have lunch. The army has " .$this->foodAmount. " meals left!<br>");
        }
    }

}
?>
<?php
require("army.php");

class game {
    
    private $turn; 
    private $armyA;
    private $armyB;

    public function __construct() {
        $this->turn = "A";
        $this->armyA = new army("A", $_GET["army1"]);
        $this->armyB = new army("B", $_GET["army2"]);

    }    

  private function changeTurn() {
        if ($this->turn == "A") {
            $this->turn = "B";
        } else {
            $this->turn = "A";
            }
    }

   private function coinToss($chance) {
       return (rand(0, 100) < ($chance * 100));
   }

   /* calculates the attacking army's destructive power 
   depending on the ratio of attacking and defending army's size */
   private function calculateDestructivePower($attacker, $defender){
       $attackerSize = $attacker->getSize();
       $defenderSize = $defender->getSize();
       if ( $attackerSize > $defenderSize) {
           return 1 - ($defenderSize/$attackerSize)*0.5;
       } else {
           return ($attackerSize/$defenderSize)*0.5;
       }
     }

   private function activateTurn($attacker) {
       if ($this->armyA == $attacker) {
           $defender = $this->armyB; 
       } else {
           $defender = $this->armyA;
       }

       echo ("It's army " .$attacker->getName(). "'s turn!<br>");

       $attacker->eatLunch();

       if ($attacker->getSize() > 0) {
       
           $pickedUnit = $attacker->pickUnit();
           echo ($pickedUnit->getName() . " is attacking.<br>");
  
           $weather = new weather();
           $weatherReport = $weather->getWeatherReport();
           echo ("The weather is " .$weatherReport. ". ");     

           $power = $this->calculateDestructivePower($attacker, $defender);
           $chanceOfSuccess = $pickedUnit->calculateSuccess($weatherReport, $power);
           echo(" Chance of success: " .$chanceOfSuccess .".<br>" );
  
           if ($this->coinToss($chanceOfSuccess)==TRUE) {
               echo ("Success!<br>");
               $defender->deleteSoldiers();
           }  else {
               echo ("The attack failed!<br>");
           }
  
       }
  }

 
   public function playGame() {
    
       while ($this->armyA->getSize() > 0 && $this->armyB->getSize() > 0) {
         
        if ($this->turn == "A") {
            $this->activateTurn($this->armyA);
        } else {
            $this->activateTurn($this->armyB);
        }

        echo("Army A has " .$this->armyA->getSize()." soldiers. Army B has " .$this->armyB->getSize()." soldiers. <br><br>");
         
        $this->changeTurn();        
      }

      echo("GAME OVER");

   }
}


?>
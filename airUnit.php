<?php
require_once("unit.php");

class airUnit extends unit {


    public function __construct() {
        $this->setName("Air unit");
        $this->setWeatherCoefficients ( array(
        "snowy" => 0.7,
        "sunny" => 1.0,
        "foggy" => 0.2,
            ));
     
    }

    protected function printStatement($weather) {
     switch ($weather) {
            case "snowy":
                echo("The planes aren't too bothered by the snow, but it's still risky to fly in these conditions.");
                break;
            case "sunny":
                echo("Perfect weather for flying a plane!");
                break;
            case "foggy":
                echo("Maybe the planes shouldn't take off...");
                break;
            default:
                echo("This type of weather isn't implemented! ");

        }   
        
    }
}

?>
<?php
    
require_once("unit.php");

class landUnit extends unit {
    
    public function __construct() {
        $this->setName("Land unit");
        $this->setWeatherCoefficients ( array(
        "snowy" =>0.2,
        "sunny" => 0.7,
        "foggy" => 1.0,
            ));
     
    }

   protected function printStatement($weather)  {
     switch ($weather) {
            case "snowy":
                echo("The soldiers will get sick in the cold weather.");
                break;
            case "sunny":
                echo("A nice weather for an attack, but the other army can notice you coming from miles away.");
                break;
            case "foggy":
                echo("Perfect weather for an ambush!");
                break;
            default:
                echo("This type of weather isn't implemented! ");       
      }   
        
    }
}

?>